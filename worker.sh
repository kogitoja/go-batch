#!/bin/bash

echo "Hello, Batch!"
echo "local mount path: $EFS_MOUNT_PATH"
echo "file system ID: $FILE_SYSTEM_ID"

yum install -y amazon-efs-utils nfs-utils

echo "/sbin/mount.efs:"
cat /sbin/mount.efs
echo "adding to fstab"
echo "$FILE_SYSTEM_ID:/ $EFS_MOUNT_PATH efs tls,_netdev" >> /etc/fstab
echo "mounting..."
mount -a -t efs defaults
echo "mounted"

df -h
