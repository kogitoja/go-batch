.PHONY: build clean build-macos

build: clean
	env GOOS=linux go build -o bin/worker main.go

build-macos: clean
	go build -o bin/worker main.go

clean:
	rm -rf ./bin
