FROM amazonlinux

COPY worker.sh /usr/worker.sh

RUN yum install -y curl ca-certificates && rm -rf /var/cache/apk/*

ENTRYPOINT /usr/worker.sh
